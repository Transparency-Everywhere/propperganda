var express = require('express'), mysql = require("mysql"), config = require('./config.js');
var plotly = require('plotly')("Niczem", "xwfeu4wtz6");
var connection = mysql.createConnection({

                          host     : config.db_host,
                          user     : config.db_user,
                          password : config.db_password,
                          database : config.db_database,
                          port     : config.db_port, 
});

connection.connect(function(err) {
    console.log('db error'+err);
});

var result = '';
var d = 1;
var dayDB = new function(){

  this.loadArticleCounts = function(startstamp, stopstamp, callback){
    
    var queryString = 'SELECT * FROM `days` WHERE date BETWEEN ? AND ?';
    connection.query(queryString, [startstamp, stopstamp], function(err, rows, fields) {
        if (err) throw err;
        callback(rows);
        if(typeof next === 'function')
            next();
    });
  };
};

dayDB.loadArticleCounts(new Date('01-01-2000').getTime()/1000, new Date('12-31-2015').getTime()/1000, function(data){
  console.log(data);
  var result = [];
  var x = [];
  var y = [];
  for(var i in data){
    x.push(new Date(data[i]['date']*1000-6200).toDateString());
    y.push(data[i]['number']);
  }

  var data = [{x:x, y:y, type: 'scatter'}];
  var layout = {fileopt : "new", filename : "final"};
  console.log(x);
  plotly.plot(data, layout, function (err, msg) {
    if (err) return console.log(err);
    console.log(msg);
  });
});
