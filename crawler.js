//        This file is published by transparency - everywhere with the best deeds.
//        Check transparency - everywhere.com for further information.
//        Licensed under the CC License, Version 4.0 (the "License");
//        you may not use this file except in compliance with the License.
//        You may obtain a copy of the License at
//        
//        https://creativecommons.org/licenses/by/4.0/legalcode
//        
//        Unless required by applicable law or agreed to in writing, software
//        distributed under the License is distributed on an "AS IS" BASIS,
//        WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//        See the License for the specific language governing permissions and
//        limitations under the License.
//        @author nicZem for Tranpanrency - everywhere.com
        
var http = require('http');
var mysql      = require('mysql');
var moment = require("moment");
var cheerio = require("cheerio");
var Crawler = require("crawler");
var url = require('url');
var trim = require('trim');
var config = require('./config.js');


function md5(str){
    var crypto = require('crypto');
    return crypto.createHash('md5').update(str).digest('hex');
}


var c = new Crawler({
    maxConnections : 10
});


var connection = mysql.createConnection({
                          host     : config.db_host,
                          user     : config.db_user,
                          password : config.db_password,
                          database : config.db_database,
                          port     : config.db_port
});

connection.connect(function(err) {
            console.log('db error'+err);
                
        });

var db = new function(){
    this.saveArticle = function(post){
        var query = connection.query('INSERT INTO news_articles SET ?', post, function(err, result) { 
                
        });
    };
    
    this.getSource = function(source_id, callback){
        var query = connection.query('SELECT * FROM news_sources WHERE id = ?', source_id, function(err, result) { 
                if(callback)
                callback(result);
        });
    };
    this.updateSource = function(source_id, post){
        var query = connection.query('UPDATE news_sources SET ? WHERE ?', [post, { id: source_id }]);
    };
    this.getArticleByKeyowords = function(keywords){

        var query = connection.query('SELECT * FROM `news_articles` WHERE `title` LIKE '%putin%' AND `teaser` LIKE '%putin%' AND `text` LIKE '%putin%'', post, function(err, result) { 
                
        });
    }


    this.removeDuplicates = function(source_id){
        this.getSource(source_id, function(data){
        var query = connection.query('DELETE t1 FROM `news_articles` t1 , `news_articles` t2 WHERE  t2.link = t1.link AND t2.id > t1.id', {}, function(err, result) { 
            try {
                  console.log(result);
            }
            catch(err) {
                   console.log(error);
                   console.log(err);
            }
        });
    });


//        
        
    }
};

var sources = {1:{
     'id':1,
    'title':'Spiegel Online',
    'host':'www.spiegel.de',
    'overview_path':'/nachrichtenarchiv/artikel-DATESTRING.html',
    'date_format': 'DD.MM.YYYY',
    returnOverviewDate : function(timestamp){

          return moment(timestamp*1000).format('DD.MM.YYYY');



    },
    'overview_list_container': '#content-main .column-wide ul li',
    'overview_link_selector': "a",
    
    article_title : function($){
        return $('.article-title').text();
    },
    article_teaser: function($){
        return $('.article-intro').text();
    },
    article_text: function($){
        return $('.article-section').children().not('.article-function-social-media').not('.adition').text();
    },
    article_time: function($){
        return new Date($('time').attr('datetime')).getTime()/1000;
    },
    language:'DE'
},
2:{
     'id':2,
    'title':'FAZ',
    'host':'www.faz.net',
    'overview_path':'/artikel-chronik/nachrichten-DATESTRING/',
    'date_format': 'DD.MM.YYYY',
    returnOverviewDate : function(timestamp){
        var ArrayMonatText = new Array(); 
        ArrayMonatText[0]="januar"; 
        ArrayMonatText[1]="februar"; 
        ArrayMonatText[2]="maerz"; 
        ArrayMonatText[3]="april"; 
        ArrayMonatText[4]="mai"; 
        ArrayMonatText[5]="juni"; 
        ArrayMonatText[6]="juli"; 
        ArrayMonatText[7]="august"; 
        ArrayMonatText[8]="september"; 
        ArrayMonatText[9]="oktober"; 
        ArrayMonatText[10]="november"; 
        ArrayMonatText[11]="dezember";

          var date = moment(timestamp*1000).format('YYYY')+'-';
          date+=  ArrayMonatText[(moment(timestamp*1000).format('MM')-1)]+'-';
          date+= moment(timestamp*1000).format('DD');

          return date;


    },
    'overview_list_container': '#FAZContentLeftInner .Teaser620',
    'overview_link_selector': "a",
    
    article_title : function($){
        return $('#artikelEinleitung h2').text();
    },
    article_teaser: function($){
        return $('#artikelEinleitung p').text();
    },
    article_text: function($){
        return $('.FAZArtikelText').first().text();
    },
    article_time: function($){
        return new Date($('.lastUpdated').attr('content')).getTime()/1000;
    },
    language:'DE'
},
2:{
     'id':2,
    'title':'FAZ',
    'host':'www.faz.net',
    'overview_path':'/artikel-chronik/nachrichten-DATESTRING/',
    'date_format': 'DD.MM.YYYY',
    returnOverviewDate : function(timestamp){
        var ArrayMonatText = new Array(); 
        ArrayMonatText[0]="januar"; 
        ArrayMonatText[1]="februar"; 
        ArrayMonatText[2]="maerz"; 
        ArrayMonatText[3]="april"; 
        ArrayMonatText[4]="mai"; 
        ArrayMonatText[5]="juni"; 
        ArrayMonatText[6]="juli"; 
        ArrayMonatText[7]="august"; 
        ArrayMonatText[8]="september"; 
        ArrayMonatText[9]="oktober"; 
        ArrayMonatText[10]="november"; 
        ArrayMonatText[11]="dezember";

          var date = moment(timestamp*1000).format('YYYY')+'-';
          date+=  ArrayMonatText[(moment(timestamp*1000).format('MM')-1)]+'-';
          date+= moment(timestamp*1000).format('DD');

          return date;


    },
    'overview_list_container': '#FAZContentLeftInner .Teaser620',
    'overview_link_selector': "a",
    
    article_title : function($){
        return $('#artikelEinleitung h2').text();
    },
    article_teaser: function($){
        return $('#artikelEinleitung p').text();
    },
    article_text: function($){
        return $('.FAZArtikelText').first().text();
    },
    article_time: function($){
        return new Date($('.lastUpdated').attr('content')).getTime()/1000;
    },
    language:'DE'
}
};

var crawler = new function(){
    this.source;
    this.hrefs = [];
    this.numberOfOpenRequests;
    this.requestsDone = 0;
    this.errors = [];
    
    this.crawl = function(source){
        
        //load crawling information from sourceobject
        this.source = sources[source];
        
        
        db.getSource(source, function(data){
            //inside the source object is the unix timestmap for the last complete day
            //hich has been called. this timestamp+1day(86400s) is the first timestamp
            //that needs to bee crawled
            
            //if last_date_crawled is undefined. use first_date_to_crawl
            if(typeof data[0]['last_date_crawled'] === 'undefined')
                var first_timestamp_to_crawl = data[0]['first_date_to_crawl'];
            else
                var first_timestamp_to_crawl = data[0]['last_date_crawled']+86400;
            
            //get articles from the overviews
            crawler.getArticleOverviews(first_timestamp_to_crawl, new Date().getTime()/1000);
        });
        
        
        //unset hrefs
    };
    
    this.makeRequest = function(host, path, callback, errorCallback){
        var uri;
        path=''+path;
        if(path.indexOf('http://') > -1){
            uri = path;
        }else{
            uri = 'http://'+host+path;
        }

        c.queue([{
            uri: 'http://'+host+path,
            jQuery: {
                name: 'cheerio',
                options: {
                    normalizeWhitespace: true,
                    decodeEntities:true
                }
            },
            forceUTF8: true,
            incomingEncoding:'utf-8',

            // The global callback won't be called
            callback: function (error, result) {
                console.log(error);
                if(!error){
                //console.log($);
                var $ = cheerio.load(result.body);
                callback($);
                }else{
                    crawler.errors.push(uri);
                    errorCallback(uri);
                }
                //console.log('Grabbed', result.body.length, 'bytes');
            }
        }]);
            
    };
    
    
    this.getArticleOverviews = function(startStamp, stopStamp){
        
        crawler.numberOfOpenRequests = (stopStamp-startStamp)/86400;
        console.log(crawler.numberOfOpenRequests);
        
        finalCallback = function(hrefs){
            console.log(hrefs);
        };
        crawler.getArticleOverview(startStamp, stopStamp, finalCallback);
    };
    
    this.loadNextOverview = function(timeStamp, stopStamp, finalCallback){
        var currentStamp = timeStamp+(86400);
        if(timeStamp < stopStamp){
            console.log('load next overview');
            this.getArticleOverview(currentStamp, stopStamp, finalCallback);
        }
        
    };
    
    this.loadNextArticle = function(finalCallback){
        this.getArticle(crawler.hrefs[crawler.articleRequestsDone][1], crawler.hrefs[crawler.articleRequestsDone][0], finalCallback);
    };
    





    this.getArticleOverview = function(timestamp, stopStamp, finalCallback){
        
        var source = this.source;
        //replace datestring with date in the sources dateformat
        var date = source.returnOverviewDate(timestamp);
        
        var path = source.overview_path.replace('DATESTRING', date);
        console.log(path);
        console.log(source.host);
        console.log(source.overview_list_container);
        this.makeRequest(source.host, path, function($){
            console.log(date);
            //var $ = cheerio.load(str);
            var hrefs = [];
            $(source.overview_list_container).each(function() {
              var link = $(this).find(source.overview_link_selector);
              var text = link.text();
              var href = link.attr('href');

              hrefs.push([timestamp, href]);
            });
            crawler.articleRequestsDone = 0;
            crawler.articleNumberOfOpenRequests = hrefs.length;
           
            crawler.hrefs= hrefs;
            console.log('number of hrefs:'+hrefs.length);
            console.log(crawler.hrefs.length);
            if(crawler.hrefs.length === 0){
                crawler.loadNextOverview(timestamp, stopStamp, finalCallback);
                return null;
            }
            if(typeof crawler.hrefs[crawler.articleRequestsDone][1] === undefined){
                finalCallback(crawler.hrefs);
                return NULL
            }
            crawler.getArticle(crawler.hrefs[crawler.articleRequestsDone][1], crawler.hrefs[crawler.articleRequestsDone][0], function(){
               
            
                //add date array list to big array list
                crawler.requestsDone++;


                if(crawler.requestsDone === crawler.numberOfOpenRequests){
                    finalCallback(crawler.hrefs);
                }
                crawler.loadNextOverview(timestamp, stopStamp, finalCallback);
            });

        },function(){

                crawler.requestsDone++;


                if(crawler.requestsDone === crawler.numberOfOpenRequests){
                    finalCallback(crawler.hrefs);
                }
                crawler.loadNextOverview(timestamp, stopStamp, finalCallback);
        });
    };
    
    this.getArticle = function(href, timestamp, finalCallback){
        var source = this.source;

        this.makeRequest(source.host, href, function($){



                
                //var $ = cheerio.load(str);


                //var $ = cheerio.load(str);
                var title = source.article_title($);
                var teaser = source.article_teaser($);
                var text = source.article_text($);
                var time = source.article_time($);
                text = text.replace("if (typeof ADI != 'undefined') ADI.ad('content_ad_1');", '');
                text = text.replace("if (typeof ADI != 'undefined') ADI.ad('content_ad_2');", '');

                //here should be the mysql
                var post = {source: 2,timestamp:time,title:trim(title), teaser:trim(teaser), text:trim(text), link:href, link_hash:md5(href)};
                console.log(post.title);
                db.saveArticle(post);
                
                crawler.articleRequestsDone++;
                if(crawler.articleRequestsDone === crawler.articleNumberOfOpenRequests){
                    finalCallback();
                    
                    console.log('Updating news_sources last_date_crawled:'+timestamp);
                    db.updateSource(crawler.source.id, {last_date_crawled:timestamp});
                    console.log(crawler.errors);
                }else{
                    
                    crawler.loadNextArticle(finalCallback);
                }


        },function(){

                crawler.articleRequestsDone++;
                if(crawler.articleRequestsDone === crawler.articleNumberOfOpenRequests){
                    finalCallback();
                    console.log('finished?');
                    console.log(crawler.errors);
                }else{
                    crawler.loadNextArticle(finalCallback);
                }
        });
    };
};

//15.10.2008 error
//17.10.2008
//17.10.2008

//crawler.crawl(2);
db.removeDuplicates(1);

//http://jsfiddle.net/xfx67dkr/5/
