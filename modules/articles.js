var express = require('express'),
mysql = require("mysql"),
db = require('./db.js');


var words = require('./words.js');

var articles = function(source){
  this.database = new db();
  this.source = source;
  this.words = new words();
  this.getArticlesByKeyword = function(keyword, callback){
    var database = new db();
    // get a timestamp before running the query
    var pre_query = new Date().getTime();
    database.query("SELECT id FROM `news_articles` WHERE `source` = '"+this.source+"' AND `title` LIKE '%"+keyword+"%' AND `teaser` LIKE '%"+keyword+"%' AND `text` LIKE '%"+keyword+"%'", {}, function(result){

      // get a timestamp after running the query
      var post_query = new Date().getTime();
      // calculate the duration in seconds
      var duration = (post_query - pre_query) / 1000;
      console.log('Getting article containing the keyword "'+keyword+'"');
      console.log('Execution Time:      '+duration);
      console.log('Number of results:   '+result.length);
      callback(result);
    });
  };
  this.countWordsInText = function(string, callback){
      //replace special chars with spaces
      string = string.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g,' ');
      
//      var natural = require('natural'),
//      tokenizer = new natural.WordTokenizer();
//      console.log(tokenizer.tokenize(string));
//    var index = {},
//        words = string
//                .replace(/[.,?!;()"'-]/g, " ")
//                .replace(/\s+/g, " ")
//                .toLowerCase()
//                .split(" ");
//      console.log(words);
//      words.forEach(function (word) {
//          if (!(index.hasOwnProperty(word))) {
//              index[word] = 0;
//          }
//          index[word]++;
//      });
      var density = require('density');
      callback(density(string).getDensity());
      return null;
  };
  
  this.currentArticleCounter = 0;
  this.currentOffset = 0;
  this.currentLimit = 10;
  this.totalCount = 0;
  this.totalDone = 0;
  this.loopArticles = function(){
      
  };
  this.countWords = function(offset, limit, max, roundSize){
    var database = this.database;
    this.currentOffset = offset;
    this.currentLimit = limit;
    if(roundSize){
        this.roundSize = roundSize;
    }
    if(max){
        this.currentMax = max;
        if(!this.singleMax)
            this.singleMax = max;
        else if(max <= this.singleMax)
            this.singleMax = max;
    }
    
    console.log(max);
    var temp = this;
    console.log(offset+'-'+this.currentMax);
    if(offset >= this.currentMax){
        console.log('asdasdasdasdasdasdasdasdasd');
        return false;
    }else{
    console.log("SELECT id FROM `news_articles` WHERE `source`='"+this.source+"' LIMIT "+limit+" OFFSET "+offset+"");
    database.query("SELECT id FROM `news_articles` WHERE `source`='"+this.source+"' LIMIT "+limit+" OFFSET "+offset+"", {}, function(result){

            var i = 0;
            result.forEach(function(item){
                console.log(i);
                console.log(item.length);
                i++;
                temp.countWordsInArticle(item['id']);
            });
        
    });
    }
  };
  this.countWordsInArticle = function(article_id){
    var database = this.database;
    var words = this.words;
    // get a timestamp before running the query
    if(this.currentOffset>this.max)
        return true;
    var pre_query = new Date().getTime();
    var temp = this;
    database.query("SELECT id,title,teaser,text FROM `news_articles` WHERE id='"+article_id+"'", {}, function(result){
        
        var article_id = result[0]['id'];
        temp.countWordsInText(result[0]['title']+' '+result[0]['teaser']+' '+result[0]['text'], function(wordlist){
            var val;
                 var i = 1;
            wordlist.forEach(function(item){
                temp.totalCount++;
                words.getWordId(item['word'], function(wordId){
                    
                    console.log('word id:'+wordId)
                       
                        i++;
                    database.query("INSERT INTO words_in_article SET ?", {word_id:wordId, article_id: article_id, number:item['count']},function(result){
                        
                        temp.totalDone++;
                        console.log({word_id:wordId, article_id: article_id, count:item['count']});
                        
                        
                        console.log('count'+temp.totalCount);
                        console.log('done'+temp.totalDone);
                        
                        console.log(i);
                        console.log(wordlist.length);
                        
                             
                        if((i-1) === wordlist.length){
                            console.log('ASDASDASD');
                            temp.currentArticleCounter++;
                            console.log(temp.currentArticleCounter+'-'+temp.currentLimit);
                            if(temp.currentArticleCounter === temp.currentLimit){
                                temp.currentOffset = temp.currentOffset+temp.currentLimit;
                                temp.currentArticleCounter = 0;
                                console.log('currentOffset:'+temp.currentOffset);
                                console.log('currentLimit:'+temp.currentLimit);
                                
                                temp.countWords(temp.currentOffset, temp.currentLimit, temp.currentMax);
                            }
                        }
                                console.log('currentOffset:'+temp.currentOffset);
                                console.log('currentLimit:'+temp.currentLimit);
                                if(temp.totalCount === temp.totalDone){
                                    temp.currentOffset = temp.currentMax;
                                    temp.max = temp.currentMax+temp.roundSize;
                                    console.log('wouhuhuhuhhuhuhuhhuhuhuhuhuhuh');
                                    console.log('temp.countWords('+temp.currentOffset+', 1, '+temp.max+', '+temp.roundSize+')');
                                    temp.countWords(temp.currentOffset, 1, temp.max, temp.roundSize);
                                    return true;
                                }
                        
                        
                        
                    });
                });
            });
        });
      // get a timestamp after running the query
      var post_query = new Date().getTime();
      // calculate the duration in seconds
      var duration = (post_query - pre_query) / 1000;
      console.log('Getting articles');
      console.log('Execution Time:      '+duration);
      console.log('Number of results:   '+result.length);
    });
  };

      //get article

        //get words in article

          //each word

            //check if word exists in word db
              //if yes
                //get id
              //if not
                //insert and get insert id

            //insert into words_in_articles


};

module.exports = articles;















