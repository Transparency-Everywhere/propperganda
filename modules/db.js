var mysql = require("mysql"),
config = require('../config');




var db = function(){

  this.init = function(){
    if(typeof this.connection === 'undefined'){
      this.connection = mysql.createConnection({
                          host     : config.db_host,
                          user     : config.db_user,
                          password : config.db_password,
                          database : config.db_database,
                          port     : config.db_port
      });
      this.connection.connect(function(err) {
                          console.log('db error'+err);
      });
    }
  };
  this.query =  function(query, values, cb){
    this.init();
    this.connection.query(query, values, function(err, result) {
      if(!err){
        cb(result);
      }else{
          
        console.log('ERROR in query:'+query +'with parameters:');
        console.log('   '+values+':');
        console.log('   '+err);
        console.log('   retry');
        try{
            cb(result);
        }
        catch(err) {
            console.log(err)
        }
      }
    });
  };
};


module.exports = db;















